let courseData = [
  {
    id: "wdc001",
    name: "PHP-Laravel",
    description:
      "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Fugit soluta officia, maxime nemo dolor repellat magnam voluptate dignissimos dolorem minima deleniti porro quasi velit perspiciatis id ratione hic? Voluptas, quisquam?",
    price: 25000,
    onOffer: true,
  },
  {
    id: "wdc002",
    name: "Python-Django",
    description:
      "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Fugit soluta officia, maxime nemo dolor",
    price: 35000,
    onOffer: true,
  },
  {
    id: "wdc003",
    name: "Java-Springboot",
    description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit.",
    price: 45000,
    onOffer: true,
  },
  {
    id: "wdc004",
    name: "Node.js-Express.js",
    description:
      "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Fugit soluta officia, maxime nemo dolor repellat magnam voluptate dignissimos dolorem",
    price: 45000,
    onOffer: false,
  },
];

export default courseData;
